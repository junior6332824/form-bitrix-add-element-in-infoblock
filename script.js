$("#callback_form").submit(function(event) {
    event.preventDefault();
    $.ajax({
      type: "POST",
      url: "/action.php",
      data: $(this).serialize(),
      success: function(data) {
          $('#callback_form').addClass('hidden');
          $('.callback__title').html(data);
      }
    });    
});