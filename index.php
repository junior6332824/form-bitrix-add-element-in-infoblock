require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');

// создаем элемент заявки в инфоблоке
$el = new CIBlockElement; // подключаем класс для работы с инфоблоками
$iblock_id = 3; // тут id вашего инфоблока

// определяем массив заполняемых полей
$PROP = array();
// присваиваем значения свойствам
$PROP["EMAIL"] = $_POST['callback_email']; 
$PROP["PHONE"] = $_POST['callback_phone']; 

$arFieldsSec = array(
'IBLOCK_ID'=>$iblock_id, // id инфоблока
'NAME' => $_POST['callback_name'], // тут к примеру POST переменная name. NAME это название поля в инфоблоке
"IBLOCK_SECTION_ID" => false, // элемент лежит в корне раздела
"ACTIVE" => "Y", // элемент активен
"PROPERTY_VALUES"=> $PROP, // заполняем поля свойств
);

// добавляем через метод
$el->Add($arFieldsSec);

